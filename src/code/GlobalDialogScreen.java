package code;

import java.util.Timer;
import java.util.TimerTask;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class GlobalDialogScreen extends PopupScreen
{
	public GlobalDialogScreen(String msg, long delay, Bitmap icon)
	{
		super(new VerticalFieldManager(VERTICAL_SCROLL),Field.FOCUSABLE);
		
		HorizontalFieldManager hfm=new HorizontalFieldManager();
		
		hfm.add(new BitmapField(icon));
		hfm.add(new LabelField(msg));
		
		add(hfm);
		
		Timer timer=new Timer();
		TimerTask timerTask;
		
		timerTask=new TimerTask() {
			public void run() {
				getApplication().invokeAndWait(new Runnable() {
					public void run() {
						closeScreen();
					}
				});
			}
		};
		timer.schedule(timerTask, delay);
	}
	
	private void closeScreen()
	{
		Ui.getUiEngine().popScreen(this);
		System.exit(0);
	}
}