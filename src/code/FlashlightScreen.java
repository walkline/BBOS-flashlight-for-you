package code;

import javax.microedition.amms.control.camera.FlashControl;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VideoControl;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.Backlight;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.SystemListener2;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class FlashlightScreen extends FullScreen implements SystemListener2//, KeyListener
{
    //private final String[] encodings = Function.getVideoEncodings();
    
    private Player _player;
    private VideoControl _videoControl;
	private FlashControl _flashControl;
	
	private int backlightTimeout=Backlight.getTimeoutDefault();
    private Background bg_Transparent=BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);
    private boolean _isFullScreen=false;
    
	public FlashlightScreen()
	{
    	Application.getApplication().addSystemListener(this);

		setBackground(bg_Transparent);

        try
        {
        	_player = javax.microedition.media.Manager.createPlayer("capture://video?encoding=video/3gpp&mode=standard");// + encodings[0]);
        	_player.realize();

        	_videoControl = (VideoControl) _player.getControl("VideoControl");
        	_flashControl = (FlashControl) _player.getControl("javax.microedition.amms.control.camera.FlashControl");

        } catch(Exception e)
        {
           	if(_player != null) {_player.close();}
        	_player = null;
        }

        try {
        	final Field videoField = (Field) _videoControl.initDisplayMode(VideoControl.USE_GUI_PRIMITIVE, "net.rim.device.api.ui.Field");
        	_videoControl.setDisplaySize(1, 1);
        	_videoControl.setVisible(true);
        	_player.start();

        	add(videoField);		
		} catch (Exception e) {
           	if(_player != null) {_player.close();}
        	_player = null;
		}
    	
    	int newMode;
        switch(_flashControl.getMode())
        {
        	case FlashControl.OFF:
                newMode = FlashControl.FORCE;
                break;
            default:
                newMode = FlashControl.OFF;
        }
    	
        _flashControl.setMode(newMode);
    	Backlight.enable(true, 255);
    	
    	add(new BitmapField(Bitmap.getBitmapResource("light.png"), LabelField.HCENTER | LabelField.VCENTER));
	}
    
	protected void onVisibilityChange(boolean visible)
	{
		super.onVisibilityChange(visible);
		
		if(visible && _player!=null && _flashControl.isFlashReady())
		{
			try {
				_videoControl.setDisplaySize(1, 1);
			} catch (MediaException e) {}
			
			_videoControl.setVisible(true);
		}
	}
	
	protected boolean keyChar(char key, int status, int time)
	{
		if(key==Characters.ESCAPE)
		{
			onClose();
			return true;
		} else if(key==Characters.SPACE)
		{
			_isFullScreen=!_isFullScreen;
			if(_isFullScreen)
			{
				try {
					_videoControl.setDisplayFullScreen(true);
				} catch (MediaException e) {}
			} else {
				try {
					_videoControl.setDisplayFullScreen(false);
					_videoControl.setDisplaySize(1, 1);
				} catch (MediaException e) {}
			}
            //_videoControl.setVisible(_isFullScreen);
			//_flashControl.setMode(_isVisable ? FlashControl.FORCE : FlashControl.OFF);
		}
		
		return true; //super.keyChar(key, status, time);
	}
	
	protected boolean navigationClick(int status, int time) 
    {
		onClose();
        return true;    
    }
	
	protected boolean trackwheelClick(int status, int time)
    {        
		onClose();    
        return true;
    }
	
	protected boolean touchEvent(TouchEvent message)
	{
		if(message.getEvent()==TouchEvent.UNCLICK)
		{
			onClose();
			return true;
		}

		return super.touchEvent(message);
	}
	
    public boolean onClose()
    {
    	if(_flashControl!=null)	{_flashControl=null;}
    	if(_videoControl!=null) {_videoControl=null;}
    	if(_player != null)
    	{
    		_player.close();
    		_player = null;
    	}

    	Backlight.setTimeout(backlightTimeout);
    	Application.getApplication().removeSystemListener(this);
    	System.exit(0);
    	
    	return true;
	}

	public void batteryGood() {}
	public void batteryLow() {}
	public void batteryStatusChange(int status) {}
	public void powerOff() {}
	public void powerUp() {}
	
	public void backlightStateChange(boolean on)
	{
		if(!on) {Backlight.enable(true, 255);}
	}

	public void cradleMismatch(boolean mismatch) {}
	public void fastReset() {}
	public void powerOffRequested(int reason) {}
	public void usbConnectionStateChange(int state) {}
}