package code;

import java.util.Vector;

public class Function
{
	/**
     * 获取机器支持的所有视频编码类型表
     * @return 返回数组形式的类型表
     */
    public static String[] getVideoEncodings()
    {
        String encodingsString = System.getProperty("video.encodings");
        
        if( encodingsString == null ) {return null;}

        Vector encodings = new Vector();       
        int start = 0;
        int space = encodingsString.indexOf(' ');

        while( space != -1 )
        {
            encodings.addElement(encodingsString.substring(start, space));          
            start = space + 1;
            space = encodingsString.indexOf(' ', start);
        }        
        encodings.addElement(encodingsString.substring(start, encodingsString.length())); 
        
        String[] encodingArray = new String[encodings.size()];
        encodings.copyInto(encodingArray);
        
        return encodingArray;
    }
}