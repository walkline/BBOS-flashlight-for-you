package code;

import localization.flashlightResource;
import net.rim.device.api.i18n.ResourceBundle;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngine;
import net.rim.device.api.ui.container.MainScreen;

public class Flashlight extends UiApplication implements flashlightResource
{
	private static ResourceBundle _resources = ResourceBundle.getBundle(BUNDLE_ID, BUNDLE_NAME);
	private final String[] encodings = Function.getVideoEncodings();
	
    public static void main(String[] args)
    {
    	Flashlight theApp = new Flashlight();       
        theApp.enterEventDispatcher();
    }

    public Flashlight()
    {
    	if(!DeviceInfo.hasCamera())
		{
    		UiApplication.getUiApplication().requestBackground();
    		Screen screen=new GlobalDialogScreen(getResString(ERROR_TIPS_1), 2500, Bitmap.getPredefinedBitmap(Bitmap.EXCLAMATION));
			Ui.getUiEngine().pushGlobalScreen(screen, 1, UiEngine.GLOBAL_QUEUE);
		} else if((DeviceInfo.getBatteryStatus() & DeviceInfo.BSTAT_NO_CAMERA_FLASH)==DeviceInfo.BSTAT_NO_CAMERA_FLASH)
		{
			UiApplication.getUiApplication().requestBackground();
			Screen screen=new GlobalDialogScreen(getResString(ERROR_TIPS_2), 2500, Bitmap.getPredefinedBitmap(Bitmap.EXCLAMATION));
			Ui.getUiEngine().pushGlobalScreen(screen, 1, UiEngine.GLOBAL_QUEUE);
		} else if(encodings == null || encodings.length <= 0)
        {            
			UiApplication.getUiApplication().requestBackground();
			Screen screen=new GlobalDialogScreen(getResString(ERROR_TIPS_3), 2500, Bitmap.getPredefinedBitmap(Bitmap.EXCLAMATION));
			Ui.getUiEngine().pushGlobalScreen(screen, 1, UiEngine.GLOBAL_QUEUE);
		} else {
		    pushScreen(new BackgroundScreen());		
		}
    }    
    
    private String getResString(int key)
	{
		return _resources.getString(key);
	}
}

class BackgroundScreen extends MainScreen 
{
	public BackgroundScreen()
	{
		UiApplication.getUiApplication().requestBackground();
		
		synchronized (Application.getEventLock())
		{
			Screen screen=new FlashlightScreen();
			Ui.getUiEngine().pushGlobalScreen(screen, 1, UiEngine.GLOBAL_QUEUE);
		}
	}
}

//SKU: 0xa56c1b683f962ab8L  //blackberry_flashlight_written_by_walkline_wang